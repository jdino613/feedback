@extends('layouts.app2')

@section('title')
   Welcome
@endsection

@section('content')

<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

    <div class="w-full h-screen relative flex items-center">
      <img src="https://images.unsplash.com/photo-1488521787991-ed7bbaae773c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2550&q=80" class="absolute top-0 left-0 w-full h-full object-cover" />

      <div class="relative z-10 px-24 text-center">
        <h2 class="mb-2 text-white uppercase text-2xl md:text-5xl leading-tight tracking-wide font-normal">Feed More, Waste Less</h2>
        
        <a href="about" class="inline-block uppercase bg-white py-3 px-6 text-xs">Learn more</a>
      </div>
    </div>

    <div class="bg-gray-100 py-24 px-12 text-center">
      <h2 class="text-lg md:text-xl mb-8">Our website gives the food industry control of their surplus food, converts it back to savings for their companies, reduces carbon footprint and empowers local communities.</h2>
      <a href="/" class="text-base uppercase text-gray-500 hover:text-gray-900">Find Restaurants</a>
    </div>

    <div class="bg-white py-12 lg:py-24 px-6 lg:px-24">
      <div class="flex flex-wrap -mx-2">
        
        <div class="w-full md:w-1/3 p-2 mb-12">
          <a href="#" class="block mb-8 bg-black hover:bg-gray-700">
            <img src="https://images.squarespace-cdn.com/content/v1/5b0ee8e33917eec5fb9f9a96/1528753715120-8OB1LQ6OVNKQ1GYG9VKK/ke17ZwdGBToddI8pDm48kMVNn7FbJLPOS7OoIPa7e_t7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UV8dI35sJ0SK11zqR-Jcz2xXOWRh5NjCcVAp9to3QUlX0P5U1bFzSEwMxHQE91vu_A/ROA_4888.jpg" />
          </a>
          <a href="about" class="block text-base text-center uppercase tracking-wider text-gray-700">Save</a>
        </div>
        
        <div class="w-full md:w-1/3 p-2 mb-12">
          <a href="#" class="block mb-8 bg-black hover:bg-gray-700">
            <img src="https://media.istockphoto.com/photos/warm-food-for-the-poor-and-homeless-concept-giving-with-charity-picture-id1129316259?k=6&m=1129316259&s=612x612&w=0&h=Ih4JrPYKxkC07OlSQpdYkt-BKOXRXPPF2a8Az9WRvDw=" />
            
          </a>
          <a href="about" class="block text-base text-center uppercase tracking-wider text-gray-700">Help</a>
        </div>
        
        <div class="w-full md:w-1/3 p-2 mb-12">
          <a href="#" class="block mb-8 bg-black hover:bg-gray-700">
            <img src="https://www.panaynews.net/wp-content/uploads/2018/07/feeding-program-fr-pagcor.jpg" />
          </a>
          <a href="about" class="block text-base text-center uppercase tracking-wider text-gray-700">Feed</a>
        </div>

      </div>

      <div class="py-12">

        <h2 class="text-center uppercase text-2xl lg:text-4xl tracking-wider mb-12" id="about">About Us</h2>

        <p class="text-gray-700 mx-auto max-w-3xl text-center leading-relaxed mb-12">At FeedBack, we help eliminate last-minute restaurant food waste in Metro Manila. We connect restaurants, buffets, caterers, as well as food retailers to organizations such as (food pantries, community kitchens, charities, food banks, orphanages, and schools) to have access to quality, convenient meals. Organizations can explore nearest available meals, and they pick up their order at a time specified by the establishment.</p>

      </div>

      <div class="w-full h-64">
        <img src="https://images.unsplash.com/photo-1532629345422-7515f3d16bb6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80" class="w-full h-full object-cover" />
      </div>

      <div class="flex flex-wrap pt-12">
        <div class="w-full lg:w-1/2">
          <h3 class="uppercase text-lg lg:text-xl tracking-wider mb-6">Contact Us</h3>
        </div>
        <div class="w-full lg:w-1/2 mb-12">
          <p class="text-sm text-gray-700 tracking-wider leading-loose mb-6">
            Makati, Metro Manila<br />
            Phone +639 800 123 456
          </p>
          <a href="#" class="text-xs uppercase text-gray-500 hover:text-gray-900">Send us an email</a>
        </div>
      </div>
    </div>

    <div class="bg-black text-gray-500 px-12 py-8 flex flex-wrap">
      <div class="w-full text-center">
        Copyright © 2020 feedback.
      </div>
    </div>

@endsection