@extends('layouts.app2')

@section('title')
    HomePage
@endsection

@section('content')

<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

        <title>FeedBack</title>

    </head>
    <body class="font-sans">


        <div class="max-w-5xl mx-auto p-8 py-20 flex justify-start items-center">
            <div class="flex-1">
                <h1 class="font-display-italic font-extrabold text-5xl md:text-6xl italic leading-none mb-6">Accelerate Good.</h1>
                <p class="leading-loose max-w-xl">Good food belongs to people, not landfills. Scroll below to browse food options.</p>
            </div>
            <div class="hidden md:block w-56">
                <a href="{{url("/welcome")}}" class="ml-12 bg-yellow-400 py-6 px-12">Enter</a>
            </div>
        </div>

<div class="carousel relative shadow-2xl bg-white">
    <div class="carousel-inner relative overflow-hidden w-full">

      <!--Slide 1-->
        <input class="carousel-open" type="radio" id="carousel-1" name="carousel" aria-hidden="true" hidden="" checked="checked">
        <div class="carousel-item absolute opacity-0">
            <img src="https://images.unsplash.com/photo-1503847752244-32e931070a43?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1586&q=80" style="height: 50vh;" class="w-full object-cover block" />
        </div>

        <label for="carousel-3" class="prev control-1 w-10 h-10 ml-2 md:ml-10 absolute cursor-pointer hidden text-3xl font-bold text-black hover:text-white rounded-full bg-white hover:bg-blue-700 leading-tight text-center z-10 inset-y-0 left-0 my-auto">‹</label>
        <label for="carousel-2" class="next control-1 w-10 h-10 mr-2 md:mr-10 absolute cursor-pointer hidden text-3xl font-bold text-black hover:text-white rounded-full bg-white hover:bg-blue-700 leading-tight text-center z-10 inset-y-0 right-0 my-auto">›</label>
        
        <!--Slide 2-->
        <input class="carousel-open" type="radio" id="carousel-2" name="carousel" aria-hidden="true" hidden="">
        <div class="carousel-item absolute opacity-0">
            <img src="https://images.unsplash.com/photo-1489710020360-66e504159b43?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1651&q=80" style="height: 50vh;" class="w-full object-cover block" />
        </div>
        <label for="carousel-1" class="prev control-2 w-10 h-10 ml-2 md:ml-10 absolute cursor-pointer hidden text-3xl font-bold text-black hover:text-white rounded-full bg-white hover:bg-blue-700 leading-tight text-center z-10 inset-y-0 left-0 my-auto">‹</label>
        <label for="carousel-3" class="next control-2 w-10 h-10 mr-2 md:mr-10 absolute cursor-pointer hidden text-3xl font-bold text-black hover:text-white rounded-full bg-white hover:bg-blue-700 leading-tight text-center z-10 inset-y-0 right-0 my-auto">›</label> 
        
        <!--Slide 3-->
        <input class="carousel-open" type="radio" id="carousel-3" name="carousel" aria-hidden="true" hidden="">
        <div class="carousel-item absolute opacity-0">
            <img src="https://images.unsplash.com/flagged/photo-1570088782323-e49798c279f8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1647&q=80" style="height: 50vh;" class="h-64 w-full object-cover block" />
        </div>
        <label for="carousel-2" class="prev control-3 w-10 h-10 ml-2 md:ml-10 absolute cursor-pointer hidden text-3xl font-bold text-black hover:text-white rounded-full bg-white hover:bg-blue-700 leading-tight text-center z-10 inset-y-0 left-0 my-auto">‹</label>
        <label for="carousel-1" class="next control-3 w-10 h-10 mr-2 md:mr-10 absolute cursor-pointer hidden text-3xl font-bold text-black hover:text-white rounded-full bg-white hover:bg-blue-700 leading-tight text-center z-10 inset-y-0 right-0 my-auto">›</label>

        <!-- Add additional indicators for each slide-->
        <ol class="carousel-indicators">
            <li class="inline-block mr-3">
                <label for="carousel-1" class="carousel-bullet cursor-pointer block text-4xl text-white hover:text-blue-700">•</label>
            </li>
            <li class="inline-block mr-3">
                <label for="carousel-2" class="carousel-bullet cursor-pointer block text-4xl text-white hover:text-blue-700">•</label>
            </li>
            <li class="inline-block mr-3">
                <label for="carousel-3" class="carousel-bullet cursor-pointer block text-4xl text-white hover:text-blue-700">•</label>
            </li>
        </ol>
        
    </div>
</div>

        {{-- <img src="https://images.unsplash.com/photo-1526016650454-68a6f488910a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1334&q=80" class="w-full h-64 object-cover" /> --}}

        <div class="max-w-5xl mx-auto pt-20 p-8 flex justify-start items-center">
            <h3 class="font-display-italic font-extrabold text-5xl md:text-6xl italic leading-none mb-12 max-w-xl">Restaurants.</h3>
        </div>

        <!-- Search Bar -->
     {{-- <div class="max-w-5xl mx-auto flex justify-start items-center">
        <form class="w-full max-w-sm" action="/search" method="POST">
            @csrf
            <div class="flex items-center input-group">
                <input class="border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none"
                  type="text" name="search" class="form-control" placeholder="Search">
                <div class="input-group-append">
                    <button class="flex-shrink-0 bg-purple-600 hover:bg-purple-800 text-white font-bold py-1 px-2 rounded" type="submit">Find Restaurant</button>
                </div>
            </div>
        </form>
    </div>
 --}}
 <div class="mainPageContent">

    <div class="container py-2">
        <div class="container mx-auto flex flex-wrap pb-20">
            @foreach ($restaurants ?? '' as $restaurant)
                <div class="w-full md:w-1/2 lg:w-1/3 px-6 mb-12">
                    <img class="w-full h-64 object-cover mb-6" src="{{asset($restaurant->image)}}" alt="Card image cap">      
                        <div>
                            <a href="{{ url('/restaurant/'.$restaurant->id) }}" class="text-2xl uppercase font-display font-extrabold tracking-widest mb-1 py-1 px-3 flex uppercase hover:bg-yellow-400">{{$restaurant->name}}</a>                           
                            <p class="font-display-italic italic text-l text-black-500 mb-4 px-3">{{$restaurant->address}}</p>
                        </div>
                </div>
            @endforeach
        </div>
    </div>

</div>

<script type="text/javascript">


    const filter = () => {
        // alert('hi');
        const catId = document.querySelector('#categoryFilter').value;
        // alert(catId);

        window.location.replace('/catalog/'+catId);
    }

    const addToCart = (id, productName) =>{
        console.log(productName)
        let quantity = document.querySelector("#quantity_"+id).value;
        alert(quantity + " of product " + productName + " has been added to cart");

        // let data = new FormData;

        // data.append("_token", "{{ csrf_token() }}");
        // data.append("quantity", quantity);

        fetch("/addtocart/"+id,{
            method: "POST",
            body: {
                "_token": {{csrf_token()}},
                "quantity": quantity
            }
        }).then(res=>res.text())
        .then(res=>console.log(res))
    }

</script>


        <div class="bg-black text-gray-500 text-center text-sm py-8">
            <p>Copyright &copy; 2020 feedback.</p>
        </div>

    </body>
</html>


@endsection