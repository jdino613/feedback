@extends('layouts.app2')

@section('content')

<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

<body class="bg-gray-100" style="background-image: url(https://images.unsplash.com/photo-1496412705862-e0088f16f791?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80); opacity: 0.95; background-attachment: fixed;">

    <div class="bg-grey-lighter min-h-screen flex flex-col">
        <div class="container max-w-lg mx-auto flex-1 flex flex-col items-center justify-center px-2">
            <div class="bg-white px-6 py-4 rounded shadow-md text-black w-full">
                <h1 class="mb-6 text-lg text-gray-900 font-light flex justify-center items-center">Sign Up</h1>
                <form method="POST" action="{{ route('register') }}">
                @csrf
                    
                <fieldset class="m-2">
                    <div class="form-group row">
                        <label for="role" class="col-md-3 col-form-label text-md-right">Role</label>

                        <div class="col-md-8">
                            <select class="block border border-grey-light w-full p-3 rounded form-control @error('address') is-invalid @enderror" name="role">
                                <option value="consumer">Organization</option>
                                <option value="restaurant">Food Establishment</option>
                            </select>                               
                            @error('role')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>
                </fieldset>

                <fieldset class="m-2">
                    <div class="form-group row">
                        <label for="name" class="col-form-label col-md-3 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-8">
                            <input id="name" type="text" class="form-control block border border-grey-light w-full p-3 rounded @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>
                </fieldset>

                <fieldset class="m-2">
                    <div class="form-group row">
                        <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('Email') }}</label>

                        <div class="col-md-8">
                            <input id="email" type="email" class="block border border-grey-light w-full p-3 rounded form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>
                </fieldset>

                <fieldset class="m-2">
                    <div class="form-group row">
                        <label for="address" class="col-md-3 col-form-label text-md-right">{{ __('Address') }}</label>

                        <div class="col-md-8">
                            <input id="address" type="text" class="block border border-grey-light w-full p-3 rounded form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address">
                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>
                </fieldset>

                <fieldset class="mb-2">
                    <div class="form-group row">
                        <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-8">
                            <input id="password" type="password" class="block border border-grey-light w-full p-3 rounded form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>
                </fieldset>

                <fieldset class="mb-2">
                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-3 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                        <div class="col-md-8">
                            <input id="password-confirm" type="password" class="block border border-grey-light w-full p-3 rounded form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>

                    </div>
                </fieldset>

                    <button type="submit" class="flex justify-center items-center w-full block bg-blue-600 hover:bg-blue-500 text-white rounded-sm py-3 text-sm-middle tracking-wide">Register</button> 

                </form>
                <div class="text-center text-sm text-grey-dark mt-4">
                        By signing up, you agree to the 
                    <a class="no-underline border-b border-grey-dark text-grey-dark" href="#">
                            Terms of Service
                    </a> and 
                    <a class="no-underline border-b border-grey-dark text-grey-dark" href="#">
                            Privacy Policy
                    </a>
                <div class="text-grey-dark font-thin mt-6">
                    Already have an account? 
                    <a class="no-underline border-b border-blue text-blue" href="{{ route('login') }}">
                        Log in
                    </a>.
                </div> 
                </div>
            </div>  
            </div>
        </div>
    </div>

</body>


@endsection
