@extends('layouts.app2')

@section('content')

<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

        <title>Login</title>

    </head>
    <body class="bg-gray-100" style="background-image: url(https://images.unsplash.com/photo-1499028344343-cd173ffc68a9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80); background-attachment: fixed; opacity: 0.9;">

        
        <div class="my-12 w-full p-6 flex justify-center items-center">
            
            <div class="w-full max-w-xs">

                <div class="bg-white border p-8 shadow rounded w-full mb-6">

                    <h1 class="mb-6 text-lg text-gray-900 font-light">Login to your account</h1>

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <fieldset class="mb-4">
                            <label class="block text-sm text-gray-900 mb-2">Email address</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror block w-full rounded-sm border bg-white py-2 px-3 text-sm" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus
                            >

                             @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </fieldset>

                        <fieldset class="mb-4">
                            <div class="w-full flex justify-between items-center">
                                <label for="password" class="block text-sm text-gray-900 mb-2">Password</label>
                            </div>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror block w-full rounded-sm border bg-white py-2 px-3 text-sm" name="password" required autocomplete="current-password"
                            >
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </fieldset>

                        <div class="px-6 pt-1 pb-5 text-sm text-gray-darker font-thin">
                            <label class="form-check-label" for="remember">
                                <input class="form-check-input mr-1" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Remember me
                            </label>
                        </div>

                        <button type="submit" class="block w-full bg-blue-600 text-white rounded-sm py-3 text-sm tracking-wide">
                            Login
                        </button> 
                        
                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </form> 

                    <p class="text-center text-sm text-gray-600 font-thin">Don't have an account yet? <a href="{{ route('register') }}" class="text-blue-500 no-underline hover:underline">Register</a></p>
                </div>
                
            </div>

        </div>


    </body>
</html>




@endsection
