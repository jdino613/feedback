@extends('layouts.app2')
@section('content')


{{-- @if(Session::has("message"))
    <h4 class="alert alert-success text-align" role="alert">{{ Session::get('message') }}</h4>
@endif --}}

<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

<body class="bg-gray-100" style="background-image: url(https://images.unsplash.com/photo-1496412705862-e0088f16f791?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80); background-attachment: fixed; opacity: 0.95;">

    <div class="bg-grey-lighter my-12 flex flex-col">
        <div class="container max-w-2xl mx-auto flex-1 flex flex-col items-center justify-center px-6">
            <div class="bg-white px-6 py-4 rounded shadow-md text-black w-full">
                <h1 class="mb-6 text-lg text-gray-900 font-thin flex justify-center items-center">Account Settings</h1>
                
                <form method="POST" action="{{route('update', $user)}}" enctype="multipart/form-data">
                @csrf
                 {{ method_field('patch') }}

                <fieldset class="m-2">
                    <div class="form-group row">
                        <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Name') }}</label>
                        <div class="col-md-8">
                            <input id="name" type="name" value="{{ $user->name }}" class="form-control" name="name">
                        </div>
                    </div>
                </fieldset>

                <fieldset class="m-2">              
                    <div class="row justify-content-center mt-2 mb-4">
                        <div class="col-md-10">
                            <input type="file" class="form-control-file" name="image" id="imageFile" aria-describedby="fileHelp">
                            <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
                        </div>  
                    </div>
                </fieldset>
                
                <fieldset class="m-2">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-md-right" for="email">Email address</label>
                        <div class="col-md-8">                           
                            <input type="email" name="email" class="form-control" value="{{ $user->email }}" id="email">
                        </div>
                    </div>
                </fieldset>

                <fieldset class="m-2">
                    <div class="form-group row">
                        <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Password') }}</label>
                        <div class="col-md-8">
                            <input id="password" type="password" class="form-control" placeholder="Enter password" name="password">
                        </div>
                    </div>
                </fieldset>

                <fieldset class="m-2">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-md-right" for="password">Confirm Password</label>
                        <div class="col-md-8"> 
                            <input type="password" placeholder="Confirm password" class="form-control" type="password" name="password_confirmation" id="password">
                        </div>
                    </div>
                </fieldset>

                <div class="form-group row mb-1">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="bg-green-600 hover:bg-green-800 text-white font-bold py-3 px-4 rounded">
                                    {{ __('Save Changes') }}
                        </button>
                    </div>
                </div>
                
                </form>

            </div>
        </div>
    </div>

{{-- <form method="post" action="{{route('update', $user)}}" enctype="multipart/form-data">
    @csrf
    {{ method_field('patch') }}
    <h3>Edit Profile</h3>

<div class="profile-header-container">
                <div class="profile-header-img">
                    
                    <div class="rank-label-container">
                        <span class="label label-default rank-label">{{$user->name}}</span>
                    </div>
                </div>
            </div>

        </div>
        <div class="row justify-content-center"> --}}
           {{--  <form action="{{ url('edit/{user}') }}" method="post" enctype="multipart/form-data">
                @csrf --}}
                {{-- <div class="form-group">
                    <input type="file" class="form-control-file" name="image" id="imageFile" aria-describedby="fileHelp">
                    <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
                </div> --}}
                {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
            {{-- </form> --}}
        {{-- </div> --}}
    {{-- </div> --}}


@endsection