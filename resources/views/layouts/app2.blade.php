<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>

    <title>feedback</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'FeedBack') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/tailwind.css')}}">
  </head>

  <body class="font-sans font-thin bg-gray-200">
    <header class="bg-white px-8 py-6 flex justify-between items-center">      
      <a href="{{url("/welcome")}}" class="font-extrabold text-2xl tracking-wider">feedback</a> 
      <ul class="hidden md:flex uppercase text-sm">
        <li class="ml-8">
          <a href="{{url("/about")}}">About</a>
        </li>
        <li class="ml-8">
                <a href="{{ url('/') }}">{{ __('RESTAURANTS') }}</a>
        </li>

         @if(auth()->check() && auth()->user()->role == "consumer")

         <li class="ml-8 dropdown">
            <a id="ml-8 Dropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>YOUR ITEMS<span class="caret"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ url('/showcart') }}">
                    {{ __('CART') }}
                </a>
                <a class="dropdown-item" href="{{ url('/history') }}">
                    {{ __('View Order History') }}
                </a>
            </div>
        </li>
              
        @endif

        @guest
              <li class="ml-8">
                  <a href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="ml-8">
                                <a href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        @if(Auth::user()->role == "restaurant")  

                        <li class="ml-8 dropdown">
                            <a id="ml-8 Dropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                YOUR ITEMS<span class="caret"></span>
                            </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item" href="{{ url('/restaurant/'.Auth::user()->id) }}"
                                    >
                                    {{ __('Available Menu') }}
                            </a>
                            <a class="dropdown-item" href="{{ url('/newproduct') }}"
                                    >
                                    {{ __('Add Food') }}
                            </a>
                            <a class="dropdown-item" href="{{ url('/restaurant/'.Auth::user()->id.'/orders') }}"
                                    >
                                    {{ __('View Orders') }}
                            </a>
                        </div>
                        </li>

                        @endif
                        <li class="ml-8 dropdown">
                            <a id="ml-8 Dropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}<span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                 <a class="dropdown-item" href="{{ url('/edit/'.Auth::user()->id) }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('edit-form').submit();">
                                    {{ __('Account Settings') }}
                                </a>

                                <form id="edit-form" action="{{ url('/edit/'.Auth::user()->id) }}" style="display: none;">
                                    @csrf
                                </form>

                                @if((auth()->check() && auth()->user()->role == "restaurant") || (auth()->check() && auth()->user()->role == "consumer"))
                                {{-- <a class="dropdown-item" href="{{ url('profile/'.Auth::user()->id) }}"
                                    >
                                    {{ __('Profile') }}
                                </a>
                                <form id="edit-form" action="{{ url('profile/'.Auth::user()->id) }}" style="display: none;">
                                    @csrf
                                </form>   --}}    
                                @endif

                                <a class="dropdown-item" href="{{ route('logout') }}" id="logout" 
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
      </ul>
    </header>
 
  <div class="contentContainer">
                @yield('content')
  </div> 

   </body>
</html>