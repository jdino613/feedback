@extends('layouts.app3')

@section('title')
    Restaurants
@endsection

@section('content')

<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

    <!-- When User is a guest -->
    @if(Auth::guest())
    <body class="bg-gray-100" style="background-image: url(https://images.unsplash.com/photo-1551546785-423f456af418?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80); object-fit: cover; background-attachment: fixed;">

        <div class="py-6">
            <h2 class="text-center text-4xl px-6">{{$restaurant->name}}</h2>
            <p class="text-center md:text-sm px-6">{{$restaurant->address}}</p>
        </div>

        <div class="container max-w-4xl mx-auto pb-10 flex flex-wrap">
            @forelse($products as $product)
                <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/2 p-5 mb-4">
                    <img class="w-full h-64 object-cover rounded-lg" src="{{ asset($product->image) }}" alt="Card image cap">
                    <div class="card-body">
                        <h2 class="text-xl text-center py-4">{{$product->name}}</h2>
                    </div>
                </div>
            @empty
                <p class="text-center text-xl py-8 px-24">No food has been created for this {{$restaurant->name}}. Please try again later.</p>
            @endforelse
        </div>
    </body>

    {{$products->links()}}
    @else

    <!-- When User is logged in -->
    @if(Session::has("message"))
        <div role="alert" class="items-center px-20 py-4">
            <div class="bg-green-500 text-white font-bold rounded-t px-4 py-3">
                Success!
            </div>
            <div class="border border-t-0 border-green-400 rounded-b bg-green-100 px-4 py-3 text-green-700">
                <p>{{Session::get('message')}}</p>
            </div>
        </div>
    @endif

<body class="bg-gray-100" style="background-image: url(https://images.unsplash.com/photo-1551546785-423f456af418?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80); object-fit: cover; background-attachment: fixed;">

    <div class="py-6">
        <h2 class="text-center text-4xl px-6">{{$restaurant->name}}</h2>
        <p class="text-center md:text-sm px-6">{{$restaurant->address}}</p>
    </div>
    
    <div class="container max-w-4xl mx-auto pb-10 flex flex-wrap">
        @forelse($products as $product)
            <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/2 p-5 mb-4">
                <img class="w-full h-64 object-cover rounded-lg" src="{{ asset($product->image) }}" alt="Card image cap">
                <div class="card-body">
                    <h2 class="text-xl text-center py-4">{{$product->name}}</h2>
                    @if((Auth::user()->id) != $restaurant->id)
                        <form action="/addtocart/{{$product->id}}" method="POST">
                        @csrf
                        <div class="flex items-center justify-center">
                            <input type="number" name="quantity" class="form-control mx-3 justify-center rounded-sm border bg-white py-2 px-3" value="1">
                            <input name = "user_id" value = "{{Auth::user()->id}}" hidden>
                            <input name = "product_id" value = "{{$product->id}}" hidden>
                            <input name = "restaurant_id" value="{{$product->user_id}}"hidden>
                            <input name = "product_name" value = "{{$product->name}}" hidden>
                            <input name = "user_address" value = "{{Auth::user()->address}}" hidden>
                            <button type="submit" class="rounded flex items-center py-2 px-2" value="Order" style="background-color: #32AC71; border: none">
                            <img src="{{ asset('icons/noun_shop.png') }}" alt="Purchase Icon" 
                            class="mx-auto object-contain h-5 w-5"
                            >
                                Add to Cart
                            </button>
                        </div>
                        </form>
                    @endif
                    
                    @if((Auth::user()->id) == $restaurant->id)
                        <form method="POST" action="{{ url('product/'.$product['id']) }}">
                        @csrf
                        @method('DELETE')
                        <div class="px-24">
                            <a class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-4 px-4 rounded" href="{{url('/product/'.$product['id'].'/edit')}}" style="border: none;">Edit Product</a>
                            <input name="rest" type="hidden" value="{{ $product['user_id'] }}">
                            <input type="submit" value="Delete" class="bg-red-500 hover:bg-red-700 text-white font-bold py-3 px-4 rounded" />
                        </div>
                        </form>
                        @endif
            </div>
    </div>

    @empty
        <p class="text-center text-xl py-8 px-24">No food has been created for {{$restaurant->name}}. Please try again later.</p>
    @endforelse
    </div>

    {{$products->links()}}

    </div>
</body>
    @endif

    <script type="text/javascript">


    const filter = () => {
        // alert('hi');
        const catId = document.querySelector('#restaurantFilter').value;
        // alert(catId);

        window.location.replace('/restaurant/'+catId);
    }

    const addToCart = (id, productName) =>{
        console.log(productName)
        let quantity = document.querySelector("#quantity_"+id).value;
        alert(quantity + " of product " + productName + " has been added to cart");


        fetch("/addtocart/"+id,{
            method: "POST",
            body: {
                "_token": {{csrf_token()}},
                "quantity": quantity
            }
        }).then(res=>res.text())
        .then(res=>console.log(res))
    }

</script>

{{-- <footer class="static bottom-0 overflow-hidden">
  <div class="bg-black text-gray-500 text-center text-sm py-8">
    <p>Copyright &copy; 2020 feedback.</p>
  </div>
</footer> --}}

@endsection