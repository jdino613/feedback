@extends('layouts.app2')
@section('content')

<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

<!DOCTYPE html>
<html lang="en" class="antialiased">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Order History</title>
   </head>
   <body class="bg-gray-100 text-gray-900 tracking-wider leading-normal" style="background-image: url(https://images.unsplash.com/photo-1476224203421-9ac39bcb3327?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80); background-attachment: fixed; opacity: 0.9;">     
    <!--Container-->
    <div class="container w-full md:w-4/5 xl:w-3/5  mx-auto px-2 py-24">
            
        <!--Card-->
        <div id='recipients' class="p-8 mt-6 lg:mt-0 rounded shadow bg-white"> 

        <!--Title-->
        <h1 class="text-center font-extrabold break-normal text-black px-2 py-4 text-xl md:text-3xl">ORDER HISTORY</h1>

            <table class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                <thead class="text-2xl">
                    <tr>
                        <th>Order ID</th>
            						<th>Date</th>
            						<th>Dish</th>
            						<th>From</th>
            						{{-- <th>Payment</th>
            						<th>Order Status</th> --}}
                    </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
    						@if($order->user_id = Auth::user()->id)
    						<tr>
      						<td class="text-base">{{$order->created_at->format('U')}}-{{$order->id}}</td>
      						<td class="text-base">{{$order->created_at->diffForHumans()}}</td>
      						<td class="text-base">{{$order->product->name}}</td>
      						<td class="text-base">{{$order->product->user->name}}</td>
{{-- 					  <td>
						@foreach($order->product_id as $product)
						Name: {{$product->name}}, Qty: {{$product->pivot->quantity}}
						<br>
						@endforeach
						</td> --}}
						{{-- <td>{{$order->payment->name}}</td> --}}
						{{-- <td>{{$order->status->name}}</td>
						<td>
							@if($order->status_id == 3 || $order->status_id == 4)
							@else
								<a href="/cancelorder/{{$order->id}}" class="btn btn-danger">Cancel Order</a>
							@endif
						</td> --}}
						</tr>
						@endif
						@endforeach
                </tbody> 
            </table>               
        </div>
        <!--/Card-->
    </div>
    <!--/container-->

   </body>
</html>


@endsection