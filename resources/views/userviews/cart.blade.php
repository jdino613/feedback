@extends('layouts.app2')
@section('content')

<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

<!DOCTYPE html>
<html lang="en" class="antialiased">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Cart</title>
   </head>
   <body class="bg-gray-100 text-gray-900 tracking-wider leading-normal" style="background-image: url(https://images.unsplash.com/photo-1504113888839-1c8eb50233d3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1615&q=80); background-attachment: fixed; opacity: 0.95;">     

    <!--Container-->
    <div class="container w-full md:w-4/5 xl:w-3/5  mx-auto px-2 py-24">
            
        <!--Card-->
        <div class="p-10 mt-8 lg:mt-0 rounded shadow bg-white"> 

        <!--Title-->
        <h1 class="text-center font-extrabold break-normal text-black-600 px-2 py-4 text-xl md:text-3xl">CART</h1>

        @if($products != null)

            <table class="table-auto text-center items-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                <thead class="text-2xl uppercase">
                    <tr>
                        <th class="w-1/3 px-2 py-2">Dish Name</th>
						<th class="w-1/3 px-4 py-2">Quantity</th>
						<th class="w-1/3 px-4 py-2">Total</th>
						<th class="w-1/3 px-4 py-2"></th>
						<th class="w-1/3 px-4 py-2"></th>
                    </tr>
                </thead>
                <tbody>
                	@foreach($products as $product)
					<tr>
						<td class="border px-4 py-2 text-base">{{$product->name}}</td>
						<td class="border px-4 py-2 text-base">{{$product->quantity}}</td>
						<td class="border px-4 py-2 text-base">{{$product->subtotal}}</td>
						<td class="mx-6 px-4 text-base">
							<form action="/removeproduct/{{$product->id}}" method="POST">
								@csrf
								@method('DELETE')
								<button class="btn btn-danger text-white font-bold" type="submit">Remove Item</button>
							</form>
						</td>
					</tr>
					@endforeach
					<tr>
						<td class="border px-4 py-2 text-base"></td>
						<td class="border px-4 py-2 text-base"></td>
						<td class="border px-4 py-2 text-base">{{$total}}</td>
						<td class="px-4 text-base">
							<a href="/clearcart" class="btn btn-danger text-white font-bold">Empty Cart</a>
						</td>
						<td></td>
						
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td class="py-4 flex justify-center">
                            <form method="POST" action="{{route('OrderController.store')}}">
                                @csrf
                                <input name = "user_id" value = "{{Auth::user()->id}}" hidden>
                                <input name = "product_id" value = "{{$product->id}}" hidden>
                                <input name = "restaurant_id" value="{{$product->user_id}}"hidden>
                                <input name = "product_name" value = "{{$product->name}}" hidden>
                                
                                <input name = "user_address" value = "{{Auth::user()->address}}" hidden>
                            	<button type="submit" class="font-bold text-white flex justify-center items-center py-2 px-10" value="Order" style="background-color: #32AC71; border: none">
                                	<img class="mx-auto object-contain h-5 w-5" src="{{ asset('icons/noun_shop.png') }}" alt="Purchase Icon">
                                Place Order
                            	</button>
                            </form>
						</td>
						<td></td>
						<td></td>
					</tr>
				
                </tbody> 
            </table>               
        </div>
        <!--/Card-->
    </div>
    <!--/container-->

    @else
    	<img src="https://i.pinimg.com/originals/2e/ac/fa/2eacfa305d7715bdcd86bb4956209038.png">
		{{-- <h2 class="text-center py-5 text-3xl">Ouhh...your cart is empty!</h2> --}}
	@endif

   </body>
</html>

@endsection