@extends('layouts.app2')

@section('title')
    About Us
@endsection

@section('content')

<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

<div class="w-full py-24 px-6 bg-cover bg-no-repeat bg-center relative z-10" style="background-image: url('https://images.unsplash.com/photo-1520250868867-4428066809c3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1762&q=80'); height: 90vh;">
    <div class="container max-w-4xl mx-auto text-center py-20">
        <h1 class="text-xl leading-tight md:text-5xl text-center text-white mt-12 italic">Doing the right thing should be simple, fun & easy!</h1>
    </div>
</div>

<div class="w-full px-6 py-12 bg-white">
      <div class="container max-w-4xl mx-auto text-center pb-10">
        
        <h3 class="text-2xl md:text-5xl leading-tight text-center max-w-md mx-auto text-gray-900">
          Our Mission
        </h3>

      </div>

      <div class="container max-w-4xl mx-auto text-center flex flex-wrap items-start md:flex-no-wrap">

        <div class="my-4 w-full md:w-1/3 flex flex-col items-center justify-center px-4">
          <img src="https://i.pinimg.com/originals/34/7e/12/347e12bc724556983d4645246b09ac31.gif" class="w-full h-64 object-cover mb-6" />

          <h2 class="text-2xl leading-tight mb-2">SAVE</h2>
          <p class="mt-3 mx-auto text-base leading-normal">…our planet by reducing food waste, one of the main causes of climate change.</p>
        </div>
        
        <div class="my-4 w-full md:w-1/3 flex flex-col items-center justify-center px-4">
          <img src="https://static.vecteezy.com/system/resources/previews/000/595/173/non_2x/hand-help-logo-and-symbols-template-icons-vector.jpg" class="w-full h-64 object-cover mb-6" />

          <h2 class="text-2xl leading-tight mb-2">HELP</h2>
          <p class="mt-3 mx-auto text-base leading-normal">… everyone to have access to quality, convenient meals.</p>
        </div>
        
        <div class="my-4 w-full md:w-1/3 flex flex-col items-center justify-center px-4">
          <img src="https://lh3.googleusercontent.com/proxy/iMXWI099Be-P6BPaRrrUtabXBhil8VgPVihDJuAEWoNCXaKYLDDhWwQPhmKqw8OwNznO20Jn8uAJoe1mhlRhUWG69pkXyd4tcZtENVj3i279K2i_-NnBhiYIQ9LmbyI" class="w-full h-64 object-cover mb-6" />

          <h2 class="text-2xl leading-tight mb-2">FEED</h2>
          <p class="mt-3 mx-auto text-base leading-normal">…delicious meals for a great cause!</p>
        </div>

      </div>
    </div>

<div class="py-12">

        <h2 class="text-center uppercase text-2xl lg:text-5xl tracking-wider mb-6">A movement bigger than us</h2>

        <h2 class="text-gray-700 mx-auto max-w-5xl text-base text-center leading-relaxed mb-2">Food waste is disrespectful on so many levels: to the hungry, to the people that spend their time cooking, to our natural resources... yet is a practice that is generally accepted and this is what makes fighting it so challenging.
<br><br>
        An estimated 2,175 tons of food scraps end up in trash bins on a daily basis in Metro Manila. 2.4 million Filipino families experienced involuntary hunger at least once in the past three months. These sobering statistics give us a glimpse of the reality that families are facing when it comes to the major hunger issue we are experiencing in the Philippines. And yet, it has been estimated that each Filipino still wastes an average of 3.29 kg of rice per year, which, when totaled, would be enough to feed 4.3 million Filipinos.
<br><br>
        On a global scale, 1/3 of the food produced globally every year never reaches our plates. According to the Food and Agriculture Organization (FAO), if food waste were a country, it would be the third largest greenhouse emitter, just after the United States and China.
		There is clearly a gap between the amount of food we are able to produce as a country and the amount of food made available for our citizens to consume. That gap is actually the amount of food that we knowingly or unknowingly waste.
<br><br>
		That is the reason why feedback is conceived. We believe the solution is simple: feed more, waste less. feedback provides a platform for charities, humanitarian organizations, orphanages, food banks, low-income public schools, and community kitchens to connect with restaurants, fast food companies, buffets, caterers, and food retailers to help manage food waste from the day's food surplus. The feedback model aims to provide a triple-win solution by improving waste management, reducing its greenhouse emissions from landﬁlls and getting its edible surplus food to those in need.</h2>

</div>

<footer class="static bottom-0 overflow-hidden">
  <div class="bg-black text-gray-500 text-center text-sm py-8">
    <p>Copyright &copy; 2020 feedback.</p>
  </div>
</footer>

@endsection