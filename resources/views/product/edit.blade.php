@extends('layouts.app2')

@section('title')
    Edit Existing Dish
@endsection

@section('content')

<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

<body class="bg-gray-100" style="background-image: url(https://images.unsplash.com/photo-1447078806655-40579c2520d6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80); object-fit: cover; opacity: 0.9; background-attachment: fixed;">

    <div class="bg-grey-lighter my-20 flex flex-col">
        <div class="container max-w-2xl mx-auto flex-1 flex flex-col items-center justify-center px-6">
            <div class="bg-white px-6 py-4 rounded shadow-md text-black w-full">
                <h1 class="mt-6 mb-2 text-2xl text-gray-900 font-thin flex justify-center items-center">{{ __('Edit Existing Dish') }}</h1>

            <div class="container w-auto p-6 flex justify-center items-center">
                @if(count($errors) > 0)
                    <div class="alert">
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="POST" action="{{ url('product/'.$product->getattribute('id')) }}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                    <input name="restaurant" type="hidden" value="{{Auth::user()->id}}">
                    <div class="form-group mb-4">
                        <label for="product_name" class="block text-l text-black-900 mb-2">{{ __('Dish Name') }}</label>
                            <div class="md-4">
                                <input id="name" type="name" value="{{$product->getattribute('name')}}" class="block w-80 rounded-sm border bg-white py-2 px-3 text-sm form-control" name="name">
                            </div>
                    </div>

                    <div class="form-group md-4">
                        <label class="block text-l text-black-900 mb-2 control-label" for="image">Image</label> 
                            <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}">
                        <p id="fileHelp" class="mt-2 text-sm">Please upload a valid image file. Size of image should not be more than 2MB.</p>
                    </div>

                    <div class="form-group py-3">
                        <div class="col-md-6 offset-md-3 mt-2">
                            <button type="submit" value="Add New Food Listing" class="p-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-3 px-4 rounded">{{ __('Update Food Listing') }}</button>
                        </div>
                    </div>
                </form>
          
            </div>
        </div>
    </div>
</div>

</body>

{{-- <h3 style="text-align: center; padding-top: 40px; margin-top: 30px;">EDIT EXISTING DISH {{strtoupper(Auth::user()->name)}}</h3>
<div id="create">
<div class="container" style="padding-top: 20px; margin-top: 30px; margin-left: 27rem;">
    
    @if(count($errors) > 0)
        <div class="alert">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form method="POST" action="{{ url('product/'.$product->getattribute('id')) }}" enctype="multipart/form-data">
       @csrf
        {{method_field('PUT')}}

        <input name="restaurant" type="hidden" value="{{Auth::user()->id}}">
        <div class="form-group">
          <label class="col-md-4 control-label" for="product_name">DISH NAME</label>  
              <div class="col-md-4">
                <input value="{{old('name')}}" id="name" name="name" placeholder="DISH NAME" class="form-control input-md" required="" type="text">
            </div>
        </div>
    <div class="form-group">
        <div class="col-md-4">
        <label class="col-md-4 control-label" for="image">IMAGE</label> 
          <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}">
        </div>
    </div>
        
        <div class="form-group">        
          <div class="col-md-4">
            <button id="singlebutton" name="singlebutton" type="submit" value="Add New Food Listing" class="btn btn-primary">EDIT</button>
          </div>
        </div>
        
    </form>
    </div>

</div> --}}



    {{-- <h3>Edit existing dish</h3>
    @if(count($errors) > 0)
        <div class="alert">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="POST" action="{{ url('product/'.$product->getattribute('id')) }}">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <p>
            <label>Name: </label>
            <input type="text" name="name" value="{{$product->getattribute('name')}}"/>
        </p>
            <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}">
        
        <input type="submit" value="Submit changes" />
    </form> --}}
@endsection