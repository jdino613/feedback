@extends('layouts.app2')

@section('title')
    Add New Food Listing
@endsection

@section('content')

<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

<body class="bg-gray-100" style="background-image: url(https://images.unsplash.com/photo-1542834291-c514e77b215f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=70); object-fit: cover; opacity: 0.9; background-attachment: fixed;">

    <div class="bg-grey-lighter my-20 flex flex-col">
        <div class="container max-w-2xl mx-auto flex-1 flex flex-col items-center justify-center px-6">
            <div class="bg-white px-6 py-2 rounded shadow-md text-black w-full">
                <h1 class="mt-6 mb-4 text-2xl text-gray-900 font-thin flex justify-center items-center">Add New Food Listing For {{(Auth::user()->name)}}</h1>

            <div id="create">
                <div class="container w-auto p-6 flex justify-center items-center">
                    @if(count($errors) > 0)
                        <div class="alert">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
            
                <form method="POST" action="{{url('product')}}" enctype="multipart/form-data">
                @csrf  
                    <input name="restaurant" type="hidden" value="{{Auth::user()->id}}">
                    <div class="form-group mb-4">
                        <label class="block text-l text-black-900 mb-2" for="product_name">DISH NAME</label>  
                        <div class="md-4">
                            <input value="{{old('name')}}" id="name" name="name" placeholder="DISH NAME" class="block w-80 rounded-sm border bg-white py-2 px-3 text-sm form-control" required="" type="text">
                        </div>
                    </div>

                    <div class="form-group md-4">
                        <label class="block text-l text-black-900 mb-2 control-label" for="image">IMAGE</label> 
                        <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}">
                        <p id="fileHelp" class="text-sm mb-2">Please upload a valid image file. Size of image should not be more than 2MB.</p>
                    </div>
                    
                    <div class="form-group py-2">   
                        <div class="col-md-6 offset-md-3 mt-2">
                            <button id="singlebutton" name="singlebutton" type="submit" value="Add New Food Listing" class="bg-purple-600 hover:bg-purple-800 text-white font-bold py-2 px-4 rounded">ADD TO LISTING</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    
</body>

@endsection