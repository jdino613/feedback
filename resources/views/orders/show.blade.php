@extends('layouts.app2')

@section('title')
    Orders Show
@endsection('title')

@section('content')

<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">


    <div class="container" style="padding-top: 20px; margin-top: 20px; text-align: center;">
        
        <div role="alert" class="items-center px-20 py-4">
            <div class="bg-green-500 text-white font-bold rounded-t px-4 py-3">
                Your order has been successful! Here are your order details!
            </div>
        </div>

            @if ($order)
            
                <div class="card" style="width: 30rem; display:inline-block; margin-right: 10px; margin-top: 20px;">
                    {{-- @if ($product ?? '') --}}
                    <img class="h-48 object-contain w-full" src="https://cdn3.iconfinder.com/data/icons/flat-actions-icons-9/792/Tick_Mark_Dark-512.png" alt="Card image cap">
                    {{-- @endif --}}
                    <div class="card-body">
                        <p class="card-text">Dish Name: {{ $order->product}}</p>
                      
                        <p class="card-text">Ordered By :  {{ $order->user }}</p>
                        <p class="card-text">Pickup Address : {{ $order->address }}</p>
                        <p class="card-text">Order Date Time : {{$order->time }}</p>
                    </div>
                    
                </div>
            @else
                <p>Error no invalid order id. Please try again!</p>
            @endif
        </div>

@endsection('content')