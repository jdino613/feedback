@extends('layouts.app2')

@section('title')
    Orders Index
@endsection

@section('content')

<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

<!DOCTYPE html>
<html lang="en" class="antialiased">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Orders Index</title>
   </head>
   <body class="bg-gray-100 text-gray-900 tracking-wider leading-normal" style="background-image: url(https://images.unsplash.com/photo-1515003197210-e0cd71810b5f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80); opacity: 0.9;">     

    <!--Container-->
    <div class="container w-full md:w-4/5 xl:w-3/5  mx-auto px-2 py-24">

        <!--Card-->
        <div id='recipients' class="p-8 mt-6 lg:mt-0 rounded shadow bg-white">

            <!--Title-->
            <h1 class="text-center font-sans font-extrabold break-normal text-black px-2 py-2 mb-3 text-xl md:text-3xl">ORDER LIST</h1>    

            <table class="stripe hover text-center" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                <thead class="text-2xl">
                    <tr>
                        <th>Dish Name</th>
                        <th>Ordered By</th>
                        {{-- <th>Quantity:</th> --}}
                        <th>Order Date Time</th>
                        {{-- <th>Order Status:</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                        @if($order->product->user->id == Auth::id())
                            <tr>
                                <td class="text-base">{{$order->product->name}}</td>
                                <td class="text-base">{{$order->user->name}}</td>
                                {{-- <td>{{$order->product->quantity}}</td> --}}
                                {{-- <td>{{$order->address}}</td> --}}
                                <td class="text-base">{{$order->created_at}}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody> 
            </table>               
        </div>
        <!--/Card-->
    </div>
    <!--/container-->

   </body>
</html>

@endsection('content')