<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use App\Product;
use App\Order;
use Illuminate\Support\Facades\Auth;

//  Auth::routes() is a helper class that helps you generate all the routes required for user authentication.
Auth::routes();

Route::get('/welcome', function () {
    return view('welcome');
});


// Route::get('/admin', function(){
// 	return view('admin.dashboard');
// });


// Profile
Route::get('/profile/{id}', 'UserController@profile');

Route::get('/add_profile', 'UserController@addprofile');

Route::post('/add_profile', 'UserController@add_profile');

Route::get('/update_profile/{id}', 'UserController@updateprofile');

Route::patch('/update_profile/{id}', 'UserController@update_profile');

Route::get('edit/{user}',  ['as' => 'edit', 'uses' => 'UserController@edit']);
Route::patch('edit/{user}/update',  ['as' => 'update', 'uses' => 'UserController@update']);
Route::post('edit/{user}', 'UserController@update_image');

// Route::get('edit/{user}', 'UserController@show')->middleware('auth')->name('edit/{user}.show');
// Route::post('edit/{user}', 'UserController@update')->middleware('auth')->name('edit/{user}.update');

// Route::get('/userviews/{profile}', 'UserController@profile')
// ->name('userviews.profile')->middleware('auth')->middleware('can:update_profile,profile');
// Route::put('/userviews/{profile}/update','UserController@update_profile')
// ->name('userviews.update_profile')->middleware('auth')->middleware('can:update_profile,profile');

// Route::resource('/users', 'UserController')->except('show')->middleware('auth')->middleware('admin');

// Resource method is used to create multiple routes for the ProductController & OrderController.
Route::resource('product','ProductController');
Route::resource('orders', 'OrderController')->except(['store', 'edit', 'update', 'destroy']);

// Getting the HomeController -> index function
Route::get('/', 'HomeController@index')->name('home');

// Getting the OrderController -> show function
Route::get('/order/{id}', 'OrderController@show');

// Getting OrderController -> index function
Route::get('/restaurant/{id}/orders', 'OrderController@index');

// Getting RestaurantController -> show function
Route::get('/restaurant/{id}', 'RestaurantController@show');

// Getting ProductController -> create function -> creating new product.
Route::get('/newproduct','ProductController@create');

// Getting RestaurantController -> stats function -> Used to display restaurant statistics such as (total amount).
Route::get('/restaurant/{id}/stats', 'RestaurantController@stats');


// Posting all Orders from the OrderController store function.
Route::post('orders', 'OrderController@store')->name('OrderController.store');

// About
Route::get('about', function() {
    return view('pages.about');
});

// Cart CRUD
Route::post('/addtocart/{id}', 'ProductController@addToCart');
Route::get('/showcart', 'ProductController@showCart');

// Delete Product
Route::delete('/removeproduct/{id}', "ProductController@removeProduct");

//  Empty Cart
Route::get('/clearcart', "ProductController@emptyCart");

// Checkout
Route::get('/checkout', "OrderController@checkout");

// Orders
// Route::get('/showorders', "OrderController@showOrders");

// Orders
Route::get('/history', "OrderController@showHistory");


// FEEDBACK & REPLIES
Route::resource('/feedback','FeedbackController');
Route::resource('/feedback_replies','FeedbackReplyController');
Route::post('/feedback_replies/ajaxDelete','FeedbackReply@ajaxDelete');
