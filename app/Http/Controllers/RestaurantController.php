<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;

class RestaurantController extends Controller
{
    // RestaurantController show function is showing all the Restuarant products.
    // Only five products per page from that restuarant is being showed.
    public function show($id) {
        $products = Product::where('user_id',$id)->orderBy('id', 'desc')->paginate(5);
        $restaurant = User::find($id);
        return view('restaurant')->with('products',$products)->with('restaurant',$restaurant);
    }

    // Showing Restaurant stats
    public function stats($id) {
        $products = [];
        $total = 0;

        $restaurant = User::find($id);
        $orders = $restaurant->orders;
        $product = Product::find($id);
                // $product->quantity = $quantity;
                // $product->subtotal = $product->$quantity + $quantity;
                $products[] = $product;
                $total += $product->subtotal;
        // $total_order = $orders->sum('order');
        return view('stats')->with('restaurant',$restaurant)->with('products','total');
    }

    // public function showCart(){

    //     $products = [];
    //     $total = 0;

    //     if(Session::has('cart')){
    //         $cart = Session::get('cart');
    //         foreach($cart as $productId => $quantity){
    //             $product = Product::find($productId);
    //             $product->quantity = $quantity;
    //             $product->subtotal = $product->$quantity + $quantity;
    //             $products[] = $product;
    //             $total += $product->subtotal;
    //         }
    //     }

    //     return view('userviews.cart', compact('products', 'total'));
    // }
}
