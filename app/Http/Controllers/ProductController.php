<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use App\Product;
use Session;
use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // Index function not used.
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    // Products create function which is returning the product.create view.
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     // storing the product name, quantity and restaurant id for all new products created.
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|max:255|unique:products,name,NULL,id,user_id,'.$request->user_id,
            
            'image' => 'image',
            'restaurant' => 'exists:users,id',
        ]);

        $product = new Product();
        $product->name = $request->name;
        $product->image = $request->image;
       
        $product->user_id = $request->restaurant;
        
        // Storing the image
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = Str::slug($request->input('name')).'_'.time();
            $filePath = '/uploads/images/'.$name.'.'.$image->getClientOriginalExtension();
            $image->storeAs('/uploads/images/', $name.'.'.$image->getClientOriginalExtension(), 'public');
            $product->image = $filePath;
        }

        $product->save();
        return redirect('restaurant/'.$product->user_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // Edit
    public function edit($id)
    {
        $product = Product::find($id);
        return view('product.edit')->with('product',$product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required|max:255|unique:products,name,NULL,id,user_id,'.$request->user_id,
            
        ]);
        
        $product = Product::find($id);

        $product->name = $request->name;
        
        $product->save();
        return redirect('/restaurant/'.$product->user_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // Delete a product.
    public function destroy(Request $request, $id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect('restaurant/'.$request->rest);
    }

    // public function edit($id){
    //     $product = product::find($id);
    //     $categories = Category::all();

    //     return view('adminviews.editproduct', compact('product', 'categories'));
    // }

    // public function update($id, Request $req){
    //     $product = product::find($id);

    //     $rules = array(
    //         "name" => "required",
    //         "description" => "required",
    //         "price" => "required|numeric",
    //         "category_id" => "required",
    //         "imgPath" => "image|mimes:jpeg,jpg,png,gif,tiff,tif,bitmap,webP"
    //     );

    //     $this->validate($req, $rules);

    //     $product->name = $req->name;
    //     $product->description = $req->description;
    //     $product->price = $req->price;
    //     $product->category_id = $req->category_id;

    //     if($req->file('imgPath') != null){
    //         $image = $req->file('imgPath');
    //         $image_name = time(). ".".$image->getClientOriginalExtension();
    //         $destination = "images/";
    //         $image->move($destination, $image_name);
    //         $product->imgPath = $destination.$image_name;
    //     }

    //     $product->save();
    //     Session::flash('message', "$product->name has been updated");
    //     return redirect('/catalog');
    // }

    public function addToCart($id, Request $req){
        // Check if there is an existing session
        if(Session::has('cart')){
            $cart = Session::get('cart');
        }else{
            $cart = [];
        }

        // Check if this is the first time we'll add a product to our cart
        if(isset($cart[$id])){
            $cart[$id] += $req->quantity;
        }else{
            $cart[$id] = $req->quantity;
        }
        Session::put("cart", $cart);

        $product = product::find($id);

        Session::flash('message', "$req->quantity of $product->name successfully added to cart");

        return redirect()->back();
    }

    public function showCart(){

        // we will create a new array containing product name, price, quantity and subtotal
        // initialize an empty array
        $products = [];
        $total = 0;

        if(Session::has('cart')){
            $cart = Session::get('cart');
            foreach($cart as $productId => $quantity){
                $product = Product::find($productId);
                $product->quantity = $quantity;
                $product->subtotal = $product->$quantity + $quantity;
                $products[] = $product;
                $total += $product->subtotal;
            }
        }

        return view('userviews.cart', compact('products', 'total'));
    }


    public function removeProduct($id){
        // $cart = Session::get('cart');
        Session::forget("cart.$id");
        return back();
}

    public function emptyCart(){
        Session::forget('cart');
        return redirect()->back();
    }

    public function search(Request $req){
        $restaurants = restaurant::where('name', 'LIKE', '%' . $req->search . '%')->get();
        

        if(count($restaurants)==0){
            Session::flash('message', 'No restaurants found');
        }

        return view('/');
    }

   
}
