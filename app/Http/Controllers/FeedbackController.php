<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FeedbackController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            Comment::create([
                'user_id' => Auth::user()->id,
                'feedback' => $request->input('feedback')
            ]);

            return redirect()->route('home')->with('success','Feedback Added successfully..!');
        }else{
            return back()->withInput()->with('error','Something wrong');
        }


        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feedback $feedback
     * @return \Illuminate\Http\Response
     */
    public function show(Feedback $feedback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feedback $feedback
     * @return \Illuminate\Http\Response
     */
    public function edit(Feedback $feedback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feedback $feedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feedback $feedback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feedback $feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feedback $feedback)
    {
        if (Auth::check()) {

            $reply = Feedback_reply::where(['feedback_id'=>$feedback->id]);
            $feedback = Feedback::where(['user_id'=>Auth::user()->id, 'id'=>$feedback->id]);
            if ($reply->count() > 0 && $feedback->count() > 0) {
                $reply->delete();
                $feedback->delete();
                return 1;
            }else if($feedback->count() > 0){
                $feedback->delete();
                return 2;
            }else{
                return 3;
            }

        }    
    }


}
