<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Order Class
class Order extends Model
{
    // Returning the order to the user
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function status(){
    	return $this->belongsTo("\App\Status");
    }

    public function product(){
    	return $this->belongsTo("\App\Product");
    }

     public function products(){
        return $this->belongsToMany("\App\Product")->withPivot("quantity")
        ->withTimeStamps();
    }
}